import Sandbox from './views/Sandbox'
import HomeScreen from './views/HomeScreen'
import SignOutScreen from './views/SignOutScreen'
import CardDetailScreen from './views/CardDetailScreen'
import SelectRoleScreen from './views/SelectRoleScreen'
import PreRegisterScreen from './views/PreRegisterScreen'
import AfterRegisterScreen from './views/AfterRegisterScreen'

import { createStackNavigator } from 'react-navigation';
import { createDrawerNavigator } from "react-navigation";

const App = createStackNavigator({
  SelectRole:{
    screen:SelectRoleScreen
  },
  AfterRegister: {
    screen: AfterRegisterScreen
  },
  PreRegister: {
    screen: PreRegisterScreen
  },
  Home: {
    screen:
      createDrawerNavigator({
        Home: {
          screen: HomeScreen,
        },
        SignOut: {
          screen: SignOutScreen,
        },
      }),
  },
  CardDetail: {
    screen: CardDetailScreen
  },
  Sandbox: {
    screen: Sandbox
  }
},
  {
    initialRouteName: 'PreRegister',
    navigationOptions: {
      header: null
    }
  });

export default App;