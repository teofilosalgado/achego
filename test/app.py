from flask import Flask, render_template, request
from werkzeug import secure_filename
app = Flask(__name__)

@app.route('/upload')
def render_file():
   return render_template('upload.html')
	
@app.route('/upload', methods = ['POST'])
def upload_file():
   print("called1")
   if request.method == 'POST':
      print("called 2")
      f = request.files['photo']
      f.save(secure_filename(f.filename))
      return 'file uploaded successfully'
		
if __name__ == '__main__':
   app.run(host='0.0.0.0', debug=True, port=80)