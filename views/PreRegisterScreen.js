import React from 'react'
import Expo from 'expo'
import { View, Text, StyleSheet, Platform, TextInput, SafeAreaView, KeyboardAvoidingView, ScrollView } from 'react-native'
import { systemWeights } from 'react-native-typography'
import CustomButton from './../components/CustomButton'
import FacebookButton from './../components/FacebookButton'
import CustomTextInput from '../components/CustomTextInput';

export default class PreRegisterScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      typedText: 'Por favor digite',
      typedPassword: 'Digite a sua senha'
    }
  }

  componentWillMount() {
    Expo.SecureStore.getItemAsync("isUser").then((response) => {
      if ((response != "") && (response != null)) {
        this.props.navigation.navigate('Home', { isUser: response })
      }
      else {
      }
    })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS == 'ios') ? "padding" : null}>
          <ScrollView style={[styles.container, { flex: 1, width: "100%" }]}>
            <View style={styles.Header} />

            <Text style={styles.hiperTitle}>O IMPACTO{"\n"}ESTA AQUI</Text>

            <View style={[styles.bloc, { flex: 1, width: "100%", paddingLeft: 20, paddingRight: 20 }]}>
              <Text style={styles.title}>INSCREVA-SE</Text>
              <View style={styles.SocialButton}>
                <FacebookButton></FacebookButton>
              </View>

              <Text style={styles.title}>OU</Text>

              <View style={{ flex: 1 }}>
                <CustomTextInput
                  keyboardType='email-address'
                  placeholder='Email'
                  onChangeText={
                    (text) => {
                      this.setState(() => {
                        return {
                          typedText: text
                        };

                      })
                    }
                  }
                ></CustomTextInput>

                <CustomTextInput
                  keyboardType='default'
                  placeholder='Senha'
                  secureTextEntry={true}
                  onChangeText={(text) => {
                    this.setState(() => {
                      return {
                        typedPassword: text
                      };
                    })
                  }}
                ></CustomTextInput>

              </View>
              <View style={styles.ActionButton}>
                <CustomButton text="ACHEGAR" onPress={() => {
                  // SE EXISTEM 2 USUARIOS:
                  this.props.navigation.navigate('SelectRole', { exists: true })
                  //CASO CONTRARIO:
                  //this.props.navigation.navigate('Home', {isUser:???)
                  //onde ??? define se eh usuario ou nao
                }}></CustomButton>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  title: {
    ...systemWeights.regular,
    fontSize: 18,
    color: '#698ED9',
    marginTop: 10,
    textAlign: "center"
  },
  Header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#5EA1E0',
    height: 170,
    flex: 1
  },
  hiperTitle: {
    color: '#fff',
    marginTop: 60,
    textAlign: "center",
    ...systemWeights.bold,
    fontSize: 30

  },
  bloc: {
    marginTop: 50
  },

  SocialButton: {
    marginTop: 5,
  },

  ActionButton: {
    marginTop: 20
  },
  TextInput: {
    borderColor: '#E6E0E0',
    borderWidth: 2,
    height: 60,
    margin: 1,
    padding: 10,
    borderRadius: 5,
    fontSize: 15,
    marginTop: 15
  },
});