import React from 'react'
import Expo from 'expo'
import { View, StyleSheet, Text, SafeAreaView, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
import CustomButton from './../components/CustomButton'
import CustomTextInput from './../components/CustomTextInput'
import CustomTextBox from './../components/CustomTextBox'
import { systemWeights } from 'react-native-typography'

export default class AfterRegisterScreen extends React.Component {
  render() {
    var content;
    var value;
    if (this.props.navigation.getParam('isUser', null) == "true") {
      value = "true";
      content =
        <View style={{ backgroundColor: "#FFFFFF", paddingLeft: 20, paddingRight: 20 }}>
          <CustomTextInput style={styles.input} placeholder="Nome"></CustomTextInput>
          <CustomTextInput style={styles.input} placeholder="Telefone"></CustomTextInput>
        </View>
    }
    else {
      value = "false";
      content =
        <View style={{ backgroundColor: "#FFFFFF", paddingLeft: 20, paddingRight: 20 }}>
          <CustomTextInput style={styles.input} placeholder="Nome"></CustomTextInput>
          <CustomTextInput style={styles.input} placeholder="Local"></CustomTextInput>
          <CustomTextInput style={styles.input} placeholder="CNPJ"></CustomTextInput>
          <CustomTextBox placeholder="Descrição"></CustomTextBox>
        </View>
    }

    return (
      <SafeAreaView style={[styles.container]}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS == 'ios') ? "padding" : null}>
          <ScrollView style={[styles.container, { flex: 1, width: "100%" }]}>
            <View style={styles.header}>
              <Text style={[styles.title, systemWeights.bold]}>Falta pouco</Text>
            </View>
            {content}
            <CustomButton onPress={async () => {
              await Expo.SecureStore.setItemAsync('isUser', value);
              const token = await Expo.SecureStore.getItemAsync('isUser');
              this.props.navigation.navigate('Home', { isUser: value })
            }} text="FINALIZAR" style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}></CustomButton>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  base: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  header: {
    height: 120,
    backgroundColor: "#5ea1e0",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "#FFFFFF",
    fontSize: 32,
  },
  input: {
    marginTop: 5
  },
  button: {
    marginTop: 10
  }
});