import React from 'react'
import CustomHeader from '../components/CustomHeader.ios'
import CustomButtom from './../components/CustomButton'
import { View, Text, Platform, StyleSheet, StatusBar, TouchableNativeFeedback, TouchableHighlight } from 'react-native'
import { systemWeights } from 'react-native-typography'
import axios from 'axios';

import BackIcon from './../components/BackIcon'

export default class HomeScreen extends React.Component {
  constructor(props) {
    super();
    this.state = {
      post: {}
    }
  }

  componentDidMount() {
    const postId = this.props.navigation.getParam('id', null);
    fetch('http://192.168.43.163:9000/evento/' + postId).then((response) =>
      response.json()
    ).then((responseJson) => {
      console.log(responseJson);
      this.setState({ post: responseJson });
    })
  }

  render() {
    return (
      <View style={styles.base}>
        <CustomHeader style={(Platform.OS == 'ios') ? {} : styles.android} searchEnabled={false} title={this.state.post.descricao} leftButton={<BackIcon iconColor="#000000" onPress={() => this.props.navigation.navigate("Home")}></BackIcon>} backgroundColor="#FFFFFF" color="#000000"></CustomHeader>
        <View style={styles.container}>
          <View style={{height:150, width:"100%", backgroundColor:"#BABACA"}}>

          </View>
          <Text style={[systemWeights.bold, {fontSize:18, marginTop:5}]}>Descrição:</Text>
          <Text style={{fontSize:14}}>{this.state.post.descricao}</Text>
          <Text style={[systemWeights.bold, {fontSize:18, marginTop:5}]}>Local:</Text>
          <Text style={{fontSize:14}}>{this.state.post.local}</Text>
          <CustomButtom text="INSCREVER"></CustomButtom>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  android: {
    marginTop: -2
  },
  base: {
    flex: 1,
    flexDirection: "column",
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: "#FFFFFF"
  },
  container: {
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "column",
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    width: '100%',
    flex: 1
  },
});