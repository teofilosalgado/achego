import React from 'react'
import { StyleSheet, Text, View, Image, TouchableHighlight, TouchableNativeFeedback } from 'react-native'
import CustomButton from './../components/CustomButton'
import { systemWeights } from 'react-native-typography'

export default class SelectRoleScreen extends React.Component {

  render() {
    var title;
    var action;
    if (this.props.navigation.getParam('exists', null) == true) {
      title = <Text style={[styles.title, systemWeights.bold]}>Entrar como:</Text>
      
    }
    else {
      title = <Text style={[styles.title, systemWeights.bold]}>Eu sou:</Text>
      action = this.props.navigation.navigate('AfterRegister', { isUser: "false" })
    }
    return (
      <View style={styles.base}>
        {title}
        <TouchableHighlight style={[styles.card, { marginBottom: 20 }]} onPress={() => {
          if (this.props.navigation.getParam('exists', null) == true) {
            action = this.props.navigation.navigate('Home', { isUser: "false" })
          }
          else {
            this.props.navigation.navigate('AfterRegister', { isUser: "false" })
          }
        }}>
          <Image source={require('./../assets/org.png')} resizeMode='contain' style={styles.image} ></Image>
        </TouchableHighlight>
        <TouchableHighlight style={styles.card} onPress={() => {
          if (this.props.navigation.getParam('exists', null) == true) {
            action = this.props.navigation.navigate('Home', { isUser: "true" })
          }
          else {
            this.props.navigation.navigate('AfterRegister', { isUser: "true" })
          }
        }}>
          <Image source={require('./../assets/user.png')} resizeMode='contain' style={styles.image} ></Image>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  base: {
    flex: 1,
    padding: 20,
    backgroundColor: "#5ea1e0",
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    color: "#FFFFFF",
    fontSize: 32,
    marginBottom: 20
  },
  card: {
    width: "100%",
    height: 200,
    backgroundColor: "#8CABEF",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20
  },
  image: {
    width: "100%",
    height: 175
  }
})
