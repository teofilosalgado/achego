import Expo from 'expo';
import React from "react";
import { StyleSheet, Text, Image, Button, View } from "react-native";

export default class SignOutScreen extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Sair',
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    Expo.SecureStore.setItemAsync('isUser', "").then(() => {
      this.props.navigation.navigate('PreRegister')
    })
  }
  render() {
    return (
      <View></View>
    )
  }
}