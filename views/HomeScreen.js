import React from 'react'
import Card from './../components/Card'
import { LinearGradient, Svg } from 'expo'
import { View, Text, StyleSheet, StatusBar, Dimensions, FlatList, Platform } from 'react-native'
import { systemWeights } from 'react-native-typography'
import MenuIcon from './../components/MenuIcon'
import CustomHeader from '../components/CustomHeader'

export default class HomeScreen extends React.Component {

  constructor(props) {
    super();
    this.state = {
      data: [
        {
          "id": "0",
          "title": "lar dos idoso",
          "sub": "Sub 2",
          "body": "Corpo 2"
        },
        {
          "id": "1",
          "title": "lar das maria",
          "sub": "Sub 3",
          "body": "Corpo 3"
        },
        {
          "id": "2",
          "title": "lar das idosa",
          "sub": "lar que abriga as idosa",
          "body": "evento pra criança"
        },
      ]
    }
  }

  static navigationOptions = {
    drawerLabel: 'Home',
  };

  componentDidMount() {
    fetch('http://192.168.43.163:9000/eventos').then((response) =>
      response.json()
    ).then((responseJson) => {
      console.log(responseJson);
      //this.setState({ post: responseJson });
    })
  }

  render() {
    var content;
    if (this.props.navigation.getParam('isUser', null) == "true") {
      content = "Aqui estão os eventos que você pode ajudar:"
    }
    else {
      content = "Eventos de sua ong:"
    }
    return (
      <LinearGradient colors={["#48C4ED", "#6F84D5"]} style={[styles.base, (Platform.OS == 'ios') ? styles.ios : styles.android]}>
        <CustomHeader title="Home" leftButton={<MenuIcon iconColor="#FFFFFF" onPress={() => this.props.navigation.openDrawer()}></MenuIcon>} searchEnabled={true} color="#FFFFFF"></CustomHeader>
        <View style={[styles.container]}>
          <View style={styles.top}>
            <View style={styles.picure}>
              <Svg style={{ height: "100%", width: 100, marginLeft: -10 }} viewBox="0 0 100 100">
                <Svg.Circle cx="50" cy="50" r="50" stroke-width="3" fill="#BABACA" />
              </Svg>
            </View>
            <View styles={styles.topText}>
              <Text style={[styles.title, systemWeights.regular]}>Olá, Caio Costa</Text>
              <Text style={[styles.subtitle, systemWeights.regular]}>{content}</Text>
            </View>
          </View>
          <View style={styles.bottom}>

            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={this.state.data}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                if (item.id != (this.state.data.length - 1)) {
                  return (
                    <Card title={item.title} sub={item.sub} body={item.body} onPress={() => this.props.navigation.navigate('CardDetail', { id: item.id })}></Card>
                  );
                }
                else {
                  return (
                    <Card style={{ marginRight: 20 }} title={item.title} sub={item.sub} body={item.body} onPress={() => this.props.navigation.navigate('CardDetail', { id: item.id })}></Card>
                  );
                }
              }}
            />
          </View>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  ios: {
    height: '100%',
    width: "100%",
    flex: 1
  },
  android: {
    height: Dimensions.get("window").height - StatusBar.currentHeight, width: Dimensions.get("window").width
  },
  item: {
    alignItems: "center",
    backgroundColor: "#dcda48",
    flexGrow: 1,
    margin: 4,
    padding: 20
  },

  topText: {
    flex: 0.3
  },
  picure: {
    flex: 0.7,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: 'flex-start',
    paddingBottom: 5
  },
  base: {
    position: "absolute",
    flexDirection: "column",
    alignItems: 'stretch',
    justifyContent: 'flex-start',

  },
  container: {
    paddingBottom: 20,
    flexDirection: "column",
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    flex: 1,
  },
  top: {
    flex: 0.3,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "column",
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  bottom:
  {
    flex: 0.7,
  },
  title: {
    fontSize: 25,
    color: "#FFFFFF"
  },
  subtitle: {
    fontSize: 15,
    color: "#FFFFFF"
  }
});