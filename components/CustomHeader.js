import React from 'react'
import { Platform } from 'react-native'
import CustomHeaderIOS from './../components/CustomHeader.ios'
import CustomHeaderAndroid from './../components/CustomHeader.android'

export default class CustomHeader extends React.Component {

  header() {
    if (Platform.OS == "ios") {
      return CustomHeaderIOS
    }
    else {
      return CustomHeaderAndroid
    }
  }

  render() {
    return (
      this.header
    )
  }
}