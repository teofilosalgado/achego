import React from 'react';
import { View, Text, StyleSheet, TouchableNativeFeedback, Dimensions, TouchableHighlight, Platform } from 'react-native'
import { systemWeights } from 'react-native-typography'



export default class Card extends React.PureComponent {

  conditionalPadding() {
    if (Platform.OS == 'ios') {
      return {
        paddingBottom: 30
      }
    }
    else {
      return {
        paddingBottom: 20
      }
    }
  }

  render() {
    return (
      <TouchableHighlight onPress={this.props.onPress}>
        <View style={[styles.base, this.props.style]}>

          <View style={{ flex: 0.50, backgroundColor: "#BABACA", borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>

          </View>
          <View style={{ flex: 0.15, paddingHorizontal: 20, paddingTop: 20, paddingBottom: 10 }}>
            <Text style={[styles.title, systemWeights.regular]}>{this.props.title}</Text>
            <Text style={[styles.subtitle, systemWeights.light]}>{this.props.sub}</Text>
          </View>
          <View style={[{ flex: 0.35, paddingHorizontal: 20 }, (Platform.OS == 'ios') ? styles.ios : styles.android]}>
            <Text style={[styles.body, systemWeights.light]} ellipsizeMode="tail" numberOfLines={6}>
              {this.props.body}
            </Text>
          </View>
        </View>
      </TouchableHighlight >
    );
  }
}

const styles = StyleSheet.create({
  base: {
    backgroundColor: "#FFFFFF",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    flex: 1,
    borderRadius: 5,
    width: Dimensions.get("window").width - 40,
    marginLeft: 20
  },
  title:
  {
    fontSize: 28
  },
  subtitle:
  {
    paddingTop: 5,
    fontSize: 19,
    paddingBottom: 5
  },
  body:
  {
    fontSize: 18,
    height: "100%",
    color: "#404040"
  },
  ios:
  {
    paddingTop: 10,
  },
  android:
  {
  }
});