import React from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'

export default class CustomTextInput extends React.Component {
  render() {
    return (
      <TextInput multiline={true}
        secureTextEntry={this.props.secureTextEntry}
        style={styles.textInput}
        keyboardType={this.props.keyboardType}
        placeholder={this.props.placeholder}
        placeholderTextColor='#C9C9C9'
        onChangeText={this.props.onChangeText}
        underlineColorAndroid="transparent"
      ></TextInput>
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    borderColor: '#E6E0E0',
    borderWidth: 2,
    height: 150,
    width: "100%",
    margin: 1,
    padding: 10,
    borderRadius: 5,
    fontSize: 15,
    marginTop: 15,
    textAlignVertical: 'top'
  },
})