import React from 'react'
import { View, TouchableHighlight, TouchableNativeFeedback, Text, Platform, StyleSheet } from 'react-native'
import { systemWeights } from 'react-native-typography'

export default class Button extends React.Component {

  render() {

    if (Platform.OS === 'ios') {
      return (
        <TouchableHighlight onPress={this.props.onPress} underlayColor={'#64CDF2'}>
          <View style={[styles.button, this.props.style]}>
            <View style={{ marginLeft: 5 }}>
              {this.props.icon}
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
              <Text style={styles.text}>{this.props.text}</Text>
            </View>
          </View>
        </TouchableHighlight>
      );
    }
    else {
      return (
        <TouchableNativeFeedback onPress={this.props.onPress} background={TouchableNativeFeedback.Ripple('#64CDF2')}>
          <View style={[styles.button, this.props.style]}>
            <View style={{ marginLeft: 5 }}>
              {this.props.icon}
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
              <Text style={styles.text}>{this.props.text}</Text>
            </View>
          </View>
        </TouchableNativeFeedback>
      );
    }
  }
}

const styles = StyleSheet.create({
  button: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#4AC2ED',
    borderRadius: 5,
  },
  text: {
    color: '#fff',
    fontSize: 18,
    ...systemWeights.regular,
  }
});