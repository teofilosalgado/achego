import React from 'react'
import { View, Text, StyleSheet, StatusBar, Dimensions, FlatList, Platform } from 'react-native'

export default class FloatingActionButton extends React.Component{
  render()
  {
    return(
      <View style={styles.base}>
        Hello
      </View>
    )
  }
}

const styles = StyleSheet.create({
  base:{
    zIndex:100,
    width:56,
    width:56,
    backgroundColor:"#6F84D5",
    borderRadius:28,
    position:"absolute",
    bottom:10,
    right:10
  }
})