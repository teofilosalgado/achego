import React from "react";
import { Svg } from 'expo';
import { View, StyleSheet } from 'react-native';
export default class FacebookIcon extends React.Component {
  render() {
    return (
      <View style={[styles.buttonHolder, this.props.style]}>
        <Svg style={{ flex: 1, height: "100%" }} height={30} width={30} viewBox="0 0 256 256">
          <Svg.Path fill="#FFFFFF" d="M255.997 239.999c0,8.832 -7.169,16.001 -16.001,16.001l-223.995 -0.003c-8.832,0 -16.001,-7.169 -16.001,-16.001l0.003 -223.995c0,-8.832 7.169,-16.001 16.001,-16.001l223.995 0.003c8.832,0 16.001,7.169 16.001,16.001l-0.003 223.995z" />
          <Svg.Path id="f" fill="#3A559F" d="M175.999 255.997l0 -95.998 31.999 0 8 -40 -39.999 0 0 -16c0,-16.001 8.016,-24.001 24.002,-24.001l15.997 0.003 0 -39.999c-8,0 -17.921,0 -31.998,0 -29.398,0 -47.999,23.049 -47.999,55.999l0 24.002 -32.002 -0.004 0 40 32.002 0 0 95.998 39.998 0z" />
        </Svg>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  buttonHolder: {
    height: "100%",
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  }
});