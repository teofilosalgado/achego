import React from 'react'
import { TouchableNativeFeedback, StyleSheet, View, TouchableHighlight } from 'react-native'
import { Svg } from 'expo'

export default class MenuIcon extends React.Component {
  render() {
    return (
      <TouchableHighlight onPress={this.props.onPress}>
        <View style={styles.buttonHolder}>
          <Svg style={{ flex: 1 }} height={25} width={50} viewBox="0 0 256 256">
            <Svg.Path fill={this.props.iconColor} d="M199.111 77.432l-186.468 0c-6.954,0 -12.643,-5.689 -12.643,-12.641 0,-6.954 5.689,-12.643 12.643,-12.643l186.468 0c6.953,0 12.642,5.689 12.642,12.643 0,6.952 -5.689,12.641 -12.642,12.641z" />
            <Svg.Path fill={this.props.iconColor} d="M243.357 140.642l-230.714 0c-6.954,0 -12.642,-5.689 -12.642,-12.642 0,-6.953 5.688,-12.641 12.642,-12.641l230.714 0c6.954,0 12.643,5.688 12.643,12.641 0,6.953 -5.689,12.642 -12.643,12.642z" />
            <Svg.Path fill={this.props.iconColor} d="M142.222 203.851l-129.579 0c-6.954,0 -12.643,-5.688 -12.643,-12.641 0,-6.953 5.689,-12.642 12.643,-12.642l129.579 0c6.954,0 12.642,5.689 12.642,12.642 0,6.953 -5.688,12.641 -12.642,12.641z" />
          </Svg>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  buttonHolder: {
    height: "100%",
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  }
});