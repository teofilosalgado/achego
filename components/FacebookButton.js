import React from 'react'
import {
  View,
  TouchableHighlight,
  TouchableNativeFeedback,
  Text,
  Platform,
  StyleSheet
} from 'react-native'
import { systemWeights } from 'react-native-typography'
import FacebookIcon from './../components/FacebookIcon'
import CustomButton from './../components/CustomButton'

export default class FacebookButton extends React.Component {
  render() {
    return (
      <CustomButton style={{height:50, backgroundColor:"#3A559F"}} icon={<FacebookIcon></FacebookIcon>} text="Conectar com facebook">
      </CustomButton>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#fff',
    borderColor: '#E6E0E0',
    borderWidth: 2,
    paddingVertical: 17,
    paddingHorizontal: 80,
    borderRadius: 5,
    marginTop: 15
  },
  text: {
    color: '#E6E0E0',
    fontSize: 18,
    ...systemWeights.regular,
  }
});