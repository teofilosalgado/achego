import React from 'react'
import { TouchableNativeFeedback, TouchableHighlight, StyleSheet, View } from 'react-native'
import { Svg } from 'expo'

export default class BackIcon extends React.Component {
  render() {
    return (
      <TouchableHighlight style={this.props.style} onPress={this.props.onPress}>
        <View style={styles.buttonHolder}>
          <Svg style={{ flex: 1 }} height={25} width={50} viewBox="0 0 256 256">
            <Svg.Path fill={this.props.iconColor} d="M189.827 0.004c-4.152,-0.091 -8.177,1.441 -11.218,4.27l-123.214 111.715c-6.531,5.916 -7.031,16.007 -1.114,22.538 0.353,0.39 0.724,0.761 1.114,1.114l123.246 111.715c6.248,6.216 16.35,6.188 22.565,-0.06 6.214,-6.248 6.188,-16.352 -0.06,-22.566 -0.351,-0.349 -0.717,-0.679 -1.097,-0.994l-110.157 -99.905 110.157 -99.905c6.609,-5.829 7.242,-15.912 1.414,-22.522 -2.952,-3.348 -7.172,-5.306 -11.636,-5.4z" />
          </Svg>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  buttonHolder: {
    height: "100%",
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  }
});