import React from 'react'
import { Animated, View, Text, StyleSheet, TouchableHighlight, TouchableNativeFeedback, TextInput, Dimensions, TouchableWithoutFeedback, Platform } from 'react-native'
import { systemWeights } from 'react-native-typography'
import { Svg } from 'expo'

export default class CustomHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchOpen: false,
      searchBackgroundAnim: new Animated.Value(0),
      searchBarAnim: new Animated.Value(0),
    };
  }
  render() {
    var rightButton;
    var header;

    if (this.props.searchEnabled == true) {
      rightButton = <TouchableNativeFeedback onPress={() => this.setState(previousState => { return { searchOpen: !previousState.searchOpen }; })}>
        <View style={styles.buttonHolder}>
          <Svg style={{ flex: 1 }} height={20} width={50} viewBox="0 0 256 256">
            <Svg.Path fill="#FFFFFF" d="M112.94 30.117c-41.55,0 -75.294,33.744 -75.294,75.294 0,41.552 33.744,75.294 75.294,75.294 41.551,0 75.294,-33.742 75.294,-75.294 0,-41.55 -33.743,-75.294 -75.294,-75.294zm77.58 146.637l0 0 53.54 53.539c5.881,5.882 5.881,15.415 0,21.297 -5.882,5.88 -15.415,5.88 -21.297,0l-55.712 -55.713c-15.822,9.49 -34.332,14.947 -54.111,14.947 -58.183,0 -105.411,-47.228 -105.411,-105.413 0,-58.183 47.228,-105.411 105.411,-105.411 58.184,0 105.412,47.228 105.412,105.411 0,27.507 -10.555,52.565 -27.832,71.343z" />
          </Svg>
        </View>
      </TouchableNativeFeedback>
    }

    if (this.state.searchOpen == false) {
      header = <View style={styles.header} backgroundColor={this.props.backgroundColor}>
        <View style={{ position: "absolute", top: 0, left: 0, height: "100%" }}>
          {this.props.leftButton}
        </View>
        <View style={{ alignSelf: "center" }}>
          <Text style={[styles.headerTitle, systemWeights.regular, { color: this.props.color }]}>{this.props.title}</Text>
        </View>
        <View style={{ position: "absolute", top: 0, right: 0, height: "100%" }}>
          {rightButton}
        </View>
      </View>
    }
    else {
      Animated.timing(
        this.state.searchBackgroundAnim,
        {
          toValue: 0.5,
          duration: 100,
        }
      ).start(() => {
        Animated.timing(
          this.state.searchBarAnim,
          {
            toValue: 1,
            duration: 200,
          }
        ).start();
      });
      header =
        <View>
          <TouchableWithoutFeedback onPress={() =>
            Animated.timing(
              this.state.searchBarAnim,
              {
                toValue: 0,
                duration: 200,
              }
            ).start(() => {
              this.setState(previousState => {
                return {
                  searchOpen: !previousState.searchOpen
                };
              }, () => {
                Animated.timing(
                  this.state.searchBackgroundAnim,
                  {
                    toValue: 0,
                    delay: 200,
                    duration: 100,
                  }
                ).start()
              })
            })
          }>
            <Animated.View style={{ position: "absolute", top: 0, left: 0, height: Dimensions.get('window').height, width: Dimensions.get('window').width, backgroundColor: "#000000", zIndex: 1, opacity: this.state.searchBackgroundAnim }}>
            </Animated.View>
          </TouchableWithoutFeedback>
          <View style={[styles.header, { zIndex: 1000, paddingVertical: 5, paddingHorizontal: 5 }]}>
            <Animated.View style={{ flex: 1, backgroundColor: "#FFFFFF", borderRadius: 2, opacity: this.state.searchBarAnim }}>
              <TextInput autoFocus={true} style={{ flex: 1, paddingHorizontal: 10 }} placeholder="Pesquisar" underlineColorAndroid="transparent" />
            </Animated.View>
          </View>
        </View>
    }
    return (
      header
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 56,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitle: {
    color: "#FFFFFF",
    fontSize: 18
  },
  buttonHolder: {
    height: "100%",
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  }
});